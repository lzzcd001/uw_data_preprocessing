import os
import re
import cv2
import json
import shutil
import numpy as np
from collections import Counter
from config import data_dir_dict, object_list_filepath


def generate_rcnn_depth_dataset(dataset_path, object_list_file):
	paths = [x for x in os.walk(os.path.join(dataset_path, 'rgb'))]
	crop_pattern = re.compile('[a-zA-z_0-9/]*_crop.png$')

	# Get the map from object class name to a specific number (in alphebatical order)
	object_name_to_order = {}
	with open(object_list_file, 'r') as object_raw_list:
		object_list = object_raw_list.readlines()
		for obj_combo in enumerate(object_list):
			object_name_to_order[obj_combo[1].rstrip()] = obj_combo[0]

	rgb_test_file = open(os.path.join(dataset_path, 'test_rgb.txt'), 'w')
	depth_test_file = open(os.path.join(dataset_path, 'test_depth.txt'), 'w')
	rgb_file = open(os.path.join(dataset_path, 'train_rgb.txt'), 'w')
	depth_file = open(os.path.join(dataset_path, 'train_depth.txt'), 'w')

	for object_name in paths[0][1]:
		obj_number = object_name_to_order[object_name]
		object_path = os.path.join(dataset_path, 'rgb', object_name)
		filenames = [x for x in os.walk(object_path)][0][2]
		for filename in filenames:
			instance_regex = re.search('[a-z]_[0-9]_', filename)
			if instance_regex != None and filename[instance_regex.start() + 2] == '1':
				rgb_test_file.write(os.path.join(object_path, filename) + ' ' + str(obj_number) + '\n')
				depth_test_file.write(
					os.path.join(dataset_path, 'depth', object_name, filename)[:-8] \
					+ 'depthcrop.png' + ' ' + str(obj_number) +'\n')
			else:
				rgb_file.write(os.path.join(object_path, filename) + ' ' + str(obj_number) + '\n')
				depth_file.write(
					os.path.join(dataset_path, 'depth', object_name, filename)[:-8] \
					+ 'depthcrop.png' + ' ' + str(obj_number) +'\n')


def split():
	generate_rcnn_depth_dataset(data_dir_dict.final_dataset_dir, object_list_fname)
