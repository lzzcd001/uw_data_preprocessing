import math
import matlab.engine
from multiprocessing import Pool
from config import data_dir_dict

def extract_process_wrapper(ind_tuple):
	eng = matlab.engine.start_matlab()
	eng.addpath(eng.genpath(eng.pwd()))
	eng.UW_background_extractor(ind_tuple[0], ind_tuple[1],
		data_dir_dict.background_original_dataset_dir, data_dir_dict.final_dataset_dir)
	return ind_tuple[0]

def extract():
	total_num = 6
	process_num = 6
	pack_size = int(math.ceil(total_num / float(process_num)))
	if total_num - pack_size * process_num != 0:
		process_num += 1

	ind_tuple_list = []

	for i in xrange(process_num):
		ind_tuple_list.append(
			(i*pack_size + 1, min((i+1)*pack_size, total_num) )
			)
	p = Pool(process_num)
	print "Start"
	p.map(extract_process_wrapper, ind_tuple_list)