from lib.create_dataset import UW_object_extract_wrapper, UW_scene_extract_wrapper
from lib.split_dataset import split_dataset_left_one_inst_out

def main():
	UW_object_extract_wrapper.extract()
	UW_scene_extract_wrapper.extract()
	split_dataset_left_one_inst_out.split()

if __name__ == "__main__":
	main()
