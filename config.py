# `pip install easydict` if you don't have it
from easydict import EasyDict as edict

data_dir_dict = edict()

######

# The original dataset and the target dataset
data_dir_dict.scene_original_dataset_dir = '/home/tom/rgbd-scenes-v2/imgs';
data_dir_dict.object_original_dataset_dir = '/home/tom/rgbd-dataset';
data_dir_dict.final_dataset_dir = '/home/tom/object_slam_datasets/rcnn_hha_dataset_loo_test';
data_dir_dict.sam_dir = os.path.join(data_dir_dict.final_dataset_dir, 'sam_gt')

######

# Object label file
object_list_filepath = '/home/tom/rgbd-dataset/uwLabels.txt'
