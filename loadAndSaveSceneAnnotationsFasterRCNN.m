% Run the detector and save detections to disk
function loadAndSaveSceneAnnotationsFasterRCNN()

close all;


%%%%%%
% Change these dirs
final_dataset_dir = '/home/tom/object_slam_datasets/rcnn_hha_dataset_loo';
scene_original_dataset_dir = '/home/tom/rgbd-scenes-v2'; %NOTE: don't add /imgs in the end of this dir
object_original_dataset_dir = '/home/tom/rgbd-dataset';
addpath('/home/tom/object_slam/utils/Annotation');
addpath('/home/tom/object_slam/utils/UW');
addpath('/home/tom/object_slam/utils/matpcl');
addpath('/home/tom/object_slam/');
addpath('/usr/local/gtsam_toolbox');
%%%%%%

sam_dir = fullfile(final_dataset_dir, 'sam_gt'); % DONOT change this line
import gtsam.*



if true
count = 0;
imageList = fullfile(sam_dir,'list.txt');
imageListID = fopen(imageList, 'w');

K = [525, 0, 320; 0, 525, 240; 0, 0, 1]; % intrinsic
propogationDelta = 40;

for scene_id = 1:14
        outputLabelDir = sam_dir;
        output_scene_dir = sprintf('%s/scene_%02d',outputLabelDir,scene_id);
        if ~exist(output_scene_dir)
            mkdir(output_scene_dir)
        end
end

for scene_id = 1:14
    dataDir = sprintf(fullfile(scene_original_dataset_dir, '/imgs/scene_%02d',scene_id));
    poseFile = sprintf(fullfile(scene_original_dataset_dir, '/pc/%02d.pose',scene_id));
    poses = dlmread(poseFile);
    %poses = poses(2:end,:);
    annotationDir = fullfile(dataDir, '/annotation/index.json');
    % outputImageDir = fullfile(sam_dir, '/JPEGImages/');
    outputLabelDir = sam_dir;
    labelFile = fullfile(dataDir,'uwLabels.txt');

    % objectLabelFile = fopen('/home/tom/rgbd-dataset/uwLabelsSubset.txt');
    objectLabelFile = fopen(fullfile(object_original_dataset_dir, 'uwLabels.txt'));
    objectLabels = textscan(objectLabelFile, '%s\n');
    objectLabels = objectLabels{1};
    %% Annotation
    annotation = annotationRead(annotationDir);

    % do interpolation for non keyframes
    frames = annotation.frames;

    image_width = 640;
    image_height = 480;
    dw = 1/image_width;
    dh = 1/image_height;

    lastAnnotatedFrame = -1;

    % map of all poses
    gtsamPoses = {};

    % list of labeled categories
    labeledCategories = {};

    % plot and save
    plotAndSave = 0;

    % frame delta
    frame_delta = 0;
    frame_delta_value = 3;

    for frame_i = 1:numel(annotation.frames)

        input_image_file = sprintf('%s/%05d-color.png',dataDir, frame_i-1);
        input_image_ = imread(input_image_file);		

        quaternion = poses(frame_i,1:4);
        rotation = gtsam.Rot3.Quaternion(quaternion(1), quaternion(2),quaternion(3),quaternion(4));
        translation = gtsam.Point3(poses(frame_i,5), poses(frame_i,6), poses(frame_i,7));
        gtsamPoses{frame_i} = gtsam.Pose3(rotation, translation);

        labelWrittenFlag = 0;
    %         label_file = sprintf('%s/%05d.txt',outputLabelDir,count);
        label_file = sprintf('%s/scene_%02d/%05d.txt',outputLabelDir,scene_id, frame_i-1);
        fileID = fopen(label_file, 'w');
        if ~isempty(annotation.frames{frame_i})


          % Write corresponding labels
              polygon = annotation.frames{frame_i}.polygon;
              for polygonID = 1:length(polygon)
                X = polygon{polygonID}.x;
                Y = polygon{polygonID}.y;
%                 minX = min(X); minY = min(Y);
%                 maxX = max(X); maxY = max(Y);
                minX = min(X) + 1; minY = min(Y) + 1;
                maxX = max(X) + 1; maxY = max(Y) + 1;


                if plotAndSave == 1	
                    try
                        croppedImage = input_image_(minY:maxY, minX:maxX, :);	
                        croppedImageFile = sprintf('/data3/UWObjectDataset/trace/%d_%d.jpg',frame_i,polygonID);
                        imwrite(croppedImage, croppedImageFile);
                    catch
                        disp('err');
                    end
                end
                X = [minX minX maxX maxX];
                Y = [minY maxY maxY minY];
                objectID = polygon{polygonID}.object +1;
                category= annotation.objects{objectID}.name;
                splitted_str = strsplit(category, ':');
                category = splitted_str{1};
                objectCells = strfind(objectLabels, category);
                categoryIndex = find(~cellfun(@isempty, objectCells));
                %fprintf('%d %s\n',frame_i, category);

    % 		    x0 = (minX + maxX)/2;	    
    % 	            y0 = (minY + maxY)/2;	    
    %                     w = maxX - minX;
    % 		    h = maxY - minY;
    % 		    x0 = x0*dw;
    % 		    y0 = y0*dh;
    % 		    w = w*dw;
    % 		    h = h*dh;

                if abs(minX) > image_width || abs(minY) > image_height || abs(maxX) > image_width || abs(maxY) > image_height  || minX < 1 || minY  < 1 ||  maxX < 1 || maxY  < 1 || minX >= maxX || minY >= maxY
                    [ minX, minY, maxX, maxY]
                    continue
                end

                fprintf(fileID, '%d %f %f %f %f\n', categoryIndex-1, minX, minY, maxX, maxY);		   
                labelWrittenFlag = 1; 

               labeledCategories{categoryIndex} = [frame_i, polygonID];
            end

        end

        for category_i = 1:numel(labeledCategories)
            if ~isempty(labeledCategories{category_i})

                categoryInfo =  labeledCategories{category_i};
                if categoryInfo(1) == frame_i || abs(categoryInfo(1) - frame_i) > propogationDelta
                    continue;
                else
                    % propogate labels
                    frameID = categoryInfo(1);				
                    polygonID = categoryInfo(2);				
                    polygon = annotation.frames{frameID}.polygon{polygonID};

                    % original polygon
                        xyz = polygon.XYZ;


                    %disp('no err');	
                    % relative pose
                    originalPose = gtsamPoses{frameID};
                    currentPose = gtsamPoses{frame_i};
                    relativePose = currentPose.between(originalPose);

                    transformed_xyz = [];
                    for polygonPts_i = 1:size(xyz,1)
                        polygon_i = xyz(polygonPts_i,:);
                        if numel(polygon_i)~=3 || iscell(polygon_i)
                            continue;
                        end
                        polygon_gtsam = gtsam.Point3(polygon_i(1), polygon_i(2), polygon_i(3));
                        transformed_polygon = relativePose.transform_from(polygon_gtsam);
                        transformed_xyz = [transformed_xyz; [transformed_polygon.x(), transformed_polygon.y(), transformed_polygon.z()]];
                    end

                    if numel(transformed_xyz) == 0
                        continue;
                    end
                    projectedPolygon = K*transformed_xyz';
                    X = projectedPolygon(1,:)./projectedPolygon(3,:);					
                    Y =  projectedPolygon(2,:)./projectedPolygon(3,:);					

                    minX = floor(min(X)); minY =  floor(min(Y));
                    maxX = floor(max(X)); maxY = floor(max(Y));
                    X = [minX minX maxX maxX];
                    Y = [minY maxY maxY minY];
                if plotAndSave == 1	
                    try
                        croppedImage = input_image_(minY:maxY, minX:maxX, :);
                        croppedImageFile = sprintf('/data3/UWObjectDataset/trace/transformed_%d_%d.jpg',frame_i,polygonID);
                        imwrite(croppedImage, croppedImageFile);
                    catch
                        disp('err');
                    end
                end
                x0 = (minX + maxX)/2;
                y0 = (minY + maxY)/2;
                w = maxX - minX;
                h = maxY - minY;
%         	            x0 = x0*dw;
%                 	    y0 = y0*dh;
% 	                    w = w*dw;
%         	            h = h*dh;
                if abs(minX) > image_width || abs(minY) > image_height || abs(maxX) > image_width || abs(maxY) > image_height  || minX < 1 || minY  < 1 ||  maxX < 1 || maxY  < 1 || minX >= maxX || minY >= maxY
                    [ minX, minY, maxX, maxY]
                    continue
                end
                fprintf(fileID, '%d %f %f %f %f\n', category_i-1, minX, minY, maxX, maxY);
                labelWrittenFlag = 1; 

                end
            end
        end			
        fclose(fileID);


    % 	if(labelWrittenFlag)
    % 		frame_delta = frame_delta + 1;
    % 
    % 		if frame_delta == frame_delta_value
    % 		% Copy image
    % 	        output_file = sprintf('%s/%05d.png',outputImageDir,count);
    % 	        input_image = sprintf('%s/%05d-color.png',dataDir, frame_i-1);
    % 
    % 	        if isempty(input_image)
    %         	        continue;
    % 	        end
    % %         	copyfile(input_image, output_file);
    % 
    % 	        % Write to list
    %        		fprintf(imageListID, '%s\n', output_file);
    % 	        count = count + 1;
    % 		frame_delta = 0;
    % 		end
    % 	end


    end
end
fclose(imageListID);
end

